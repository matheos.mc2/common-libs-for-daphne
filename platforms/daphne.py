# This file is Copyright (c) 2015-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# License: BSD

from litex.build.generic_platform import *
from litex.build.xilinx import XilinxPlatform, VivadoProgrammer

# IOs ----------------------------------------------------------------------------------------------

_io = [
    
    ("mclk", 0, 
        Subsignal("p", Pins("AF5"), IOStandard("LVDS_25"),
            Misc("DIFF_TERM=TRUE")),
        Subsignal("n", Pins("AF4"), IOStandard("LVDS_25"),
            Misc("DIFF_TERM=TRUE")),
    ),
    
    ("clk625", 0, 
        Subsignal("p", Pins("AA3"), IOStandard("LVDS_25"),
            Misc("DIFF_TERM=TRUE")),
        Subsignal("n", Pins("AA2"), IOStandard("LVDS_25"),
            Misc("DIFF_TERM=TRUE")),
    ),

    ("clk100", 0, 
        Subsignal("p", Pins("AA4"), IOStandard("LVDS_25"),
            Misc("DIFF_TERM=TRUE")),
        Subsignal("n", Pins("AB4"), IOStandard("LVDS_25"),
            Misc("DIFF_TERM=TRUE")),
    ),

    ("lemo_i",0, Pins("D5"), IOStandard("LVCMOS33"),),

    ("lemo_o",0, Pins("D4"), IOStandard("LVCMOS33"),),

    ("reset", 0, Pins("J8"), IOStandard("LVCMOS33")),


    ("afe", 0, 
        Subsignal("dclkp", Pins("AA19"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("dclkn", Pins("AB19"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkp", Pins("AA20"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkn", Pins("AB20"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datap",  Pins("Y15 AB16 AA17 AE18 AC18 AF19 AD20 AD21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datan",  Pins("AA15 AC16 AB17 AF18 AD18 AF20 AE20 AE21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),

    ),

    ("afe", 1, 
        Subsignal("dclkn", Pins("C18"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("dclkp", Pins("D18"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkn", Pins("C19"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkp", Pins("D19"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datan",  Pins("A25 A24 D21 B21 A19 A18 B17 D16"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datap",  Pins("B25 A23 E21 C21 B19 A17 C17 E16"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),

    ),

    ("afe", 2, 
        Subsignal("dclkn", Pins("J21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("dclkp", Pins("K21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkn", Pins("H22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkp", Pins("H21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datan",  Pins("K23 H24 J26 G26 D25 D26 E23 F22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datap",  Pins("K22 J24 J25 H26 E25 E26 F23 G22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),

    ),

    ("afe", 3, 
        Subsignal("dclkn", Pins("N22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("dclkp", Pins("N21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkn", Pins("M22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkp", Pins("M21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datan",  Pins("T25 R23 P25 P26 M26 M25 L25 K26"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datap",  Pins("T24 T23 R25 R26 N26 M24 L24 K25"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),

    ),
    
    ("afe", 4, 
        Subsignal("dclkn", Pins("V22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("dclkp", Pins("U22"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkn", Pins("V21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("fclkp", Pins("U21"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datan",  Pins("Y20 Y23 AA23 AC24 AC26 AA25 Y26 W26"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),
        Subsignal("datap",  Pins("W20 Y22 AA22 AB24 AB26  Y25 W25 V26"),IOStandard("LVDS_25"),Misc("DIFF_TERM=TRUE")),

    ),

    
    ("cdr", 0, 
        Subsignal("cdr_clk_p", Pins("AB2"), IOStandard("LVDS_25")),
        Subsignal("cdr_clk_n", Pins("AC2"), IOStandard("LVDS_25")),
        Subsignal("cdr_p", Pins("AD3"), IOStandard("LVDS_25")),
        Subsignal("cdr_n", Pins("AC3"), IOStandard("LVDS_25")),
        Subsignal("cdr_sfp_tx_p", Pins("V6"), IOStandard("LVDS_25")),
        Subsignal("cdr_sfp_tx-n", Pins("W6"), IOStandard("LVDS_25")),
        Subsignal("f_sfp_abs", Pins("F8"), IOStandard("LVCMOS33")),
        Subsignal("f_sfp_los", Pins("G7"), IOStandard("LVCMOS33")),
        Subsignal("f_sfp_txdis", Pins("H7"), IOStandard("LVCMOS33")),
        Subsignal("f_cdr_lol", Pins("H8"), IOStandard("LVCMOS33")),
        Subsignal("f_cdr_los", Pins("D6"), IOStandard("LVCMOS33")),
    ),

    ("sfp_i2c", 0,
        Subsignal("abs", Pins("J6")),
        Subsignal("los", Pins("H6")),
        Subsignal("scl", Pins("H9")),
        Subsignal("sda", Pins("G9")),
        Subsignal("txd", Pins("G6")),
        IOStandard("LVCMOS33"),
    ),

    ("sfp_i2c", 1,
        Subsignal("abs", Pins("K7")),
        Subsignal("los", Pins("L8")),
        Subsignal("scl", Pins("J4")),
        Subsignal("sda", Pins("H4")),
        Subsignal("txd", Pins("K8")),
        IOStandard("LVCMOS33"),
    ),

    ("stm", 0,
        Subsignal("reset", Pins("J8")),
        Subsignal("scl", Pins("G4")),
        Subsignal("mosi", Pins("G5")),
        Subsignal("csn", Pins("K6")),
        Subsignal("miso", Pins("F4")),
        Subsignal("irq", Pins("F5")),
        IOStandard("LVCMOS33"),
    ),

    ("pudc_b", 0, Pins("P18"), IOStandard("LVCMOS25")),

    ("serial", 0,
        Subsignal("cts", Pins("C3")),
        Subsignal("rts", Pins("F3")),
        Subsignal("tx",  Pins("E3")),
        Subsignal("rx",  Pins("C2")),
        IOStandard("LVCMOS33")
    ),

    ("dbg", 0, Pins("C3"), IOStandard("LVCMOS33")),
    ("dbg", 1, Pins("F3"), IOStandard("LVCMOS33")),
    ("dbg", 2, Pins("E3"), IOStandard("LVCMOS33")),
    ("dbg", 3, Pins("C2"), IOStandard("LVCMOS33")),
    ("dbg", 4, Pins("B2"), IOStandard("LVCMOS33")),
    ("dbg", 5, Pins("A3"), IOStandard("LVCMOS33")),
    ("dbg", 6, Pins("A2"), IOStandard("LVCMOS33")),
    ("dbg", 7, Pins("C1"), IOStandard("LVCMOS33")),
    ("dbg", 8, Pins("B1"), IOStandard("LVCMOS33")),
    ("dbg", 9, Pins("F2"), IOStandard("LVCMOS33")),
    ("dbg", 10, Pins("E2"), IOStandard("LVCMOS33")),
    ("dbg", 11, Pins("E1"), IOStandard("LVCMOS33")),
    ("dbg", 12, Pins("D1"), IOStandard("LVCMOS33")),
    ("dbg", 13, Pins("G2"), IOStandard("LVCMOS33")),

    ("user_led", 0, Pins("C4"), IOStandard("LVCMOS33")),
    ("user_led", 1, Pins("B5"), IOStandard("LVCMOS33")),
    ("user_led", 2, Pins("A5"), IOStandard("LVCMOS33")),
    ("user_led", 3, Pins("B4"), IOStandard("LVCMOS33")),
    ("user_led", 4, Pins("A4"), IOStandard("LVCMOS33")),
    ("user_led", 5, Pins("D3"), IOStandard("LVCMOS33")),

    ("ddram", 0,
        Subsignal("a", Pins(
            "T2 R2 U2 U1 P6 P5 T5 R5 U6",
            "P8 R7 R6 T8 T7"),
            IOStandard("SSTL18_II")),
        Subsignal("ba", Pins("K1 J1 L3"), IOStandard("SSTL18_II")),
        Subsignal("ras_n", Pins("H2"), IOStandard("SSTL18_II")),
        Subsignal("cas_n", Pins("M2"), IOStandard("SSTL18_II")),
        Subsignal("we_n", Pins("N3"), IOStandard("SSTL18_II")),
        Subsignal("dm", Pins("N1 M1"), IOStandard("SSTL18_II")),
        Subsignal("dq", Pins(
            "N6 N7 K5 L5 L7 M7 J3 K3"), IOStandard("SSTL18_II"),
            Misc("IN_TERM=UNTUNED_SPLIT_50")),
        Subsignal("dqs_p", Pins("M4"), IOStandard("DIFF_SSTL18_II")),
        Subsignal("dqs_n", Pins("L4"), IOStandard("DIFF_SSTL18_II")),
        Subsignal("clk_p", Pins("R3"), IOStandard("DIFF_SSTL18_II")),
        Subsignal("clk_n", Pins("P3"), IOStandard("DIFF_SSTL18_II")),
        Subsignal("cke", Pins("L2"), IOStandard("SSTL18_II")),
        Subsignal("odt", Pins("H1"), IOStandard("SSTL18_II")),
        Subsignal("cs_n", Pins("T3"), IOStandard("SSTL18_II")),
        Misc("SLEW=FAST"),
    ),
    ("gtp_clk", 0,
        Subsignal("p", Pins("F11"), IOStandard("DIFF_SSTL18_II")),
        Subsignal("n", Pins("E11"), IOStandard("DIFF_SSTL18_II")),
    ),
    ("sfp", 0,
        Subsignal("txp", Pins("B7")),
        Subsignal("txn", Pins("A7")),
        Subsignal("rxp", Pins("B11")),
        Subsignal("rxn", Pins("A11"))
    ),
    ("sfp_tx", 0,
        Subsignal("p", Pins("B7")),
        Subsignal("n", Pins("A7")),
    ),
    ("sfp_rx", 0,
        Subsignal("p", Pins("B11")),
        Subsignal("n", Pins("A11")),
    ),

    ("sfp", 1,
        Subsignal("txp", Pins("D8")),
        Subsignal("txn", Pins("C8")),
        Subsignal("rxp", Pins("D14")),
        Subsignal("rxn", Pins("C14"))
    ),
    ("sfp_tx", 1,
        Subsignal("p", Pins("D8")),
        Subsignal("n", Pins("C8")),
    ),
    ("sfp_rx", 1,
        Subsignal("p", Pins("D14")),
        Subsignal("n", Pins("c14")),
    ),
    
]


# Platform -----------------------------------------------------------------------------------------

class Platform(XilinxPlatform):
    default_clk_name = "clk100"
    default_clk_period = 1e9/100e6

    def __init__(self):
        XilinxPlatform.__init__(self, "xc7a200t-fbg676-3", _io,  toolchain="vivado")
        self.add_platform_command("""
set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 2.5 [current_design]
set_property INTERNAL_VREF 0.9 [get_iobanks 34]
""")
        self.toolchain.bitstream_commands = \
            ["set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]"]
        self.toolchain.additional_commands = \
            ["write_cfgmem -force -format bin -interface spix4 -size 16 "
             "-loadbit \"up 0x0 {build_name}.bit\" -file {build_name}.bin"]

    def create_programmer(self):
        return VivadoProgrammer(flash_part="n25q128-3.3v-spi-x1_x2_x4")


    def load(self,build_dir="build/"):
        from litex.build.xilinx import VivadoProgrammer
        prog=VivadoProgrammer()
        prog.load_bitstream(build_dir+"top.bit")

    def do_finalize(self, fragment):
        XilinxPlatform.do_finalize(self, fragment)
        try:
            self.add_period_constraint(self.lookup_request("eth_clocks").rx, 1e9/125e6)
        except ConstraintError:
            pass

    def do_finalize(self, fragment):
        XilinxPlatform.do_finalize(self, fragment)
        self.add_period_constraint(self.lookup_request("clk100",        loose=True), 1e9/100e6)
        self.add_period_constraint(self.lookup_request("clk625",        loose=True), 1e9/62.5e6)
        self.add_period_constraint(self.lookup_request("eth_clocks:rx", loose=True), 1e9/125e6)